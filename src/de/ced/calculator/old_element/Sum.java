package de.ced.calculator.old_element;

import de.ced.calculator.PCAnDieWand;

@Deprecated
public class Sum extends ElementList {

	public Sum(Element... elements) {
		super(elements);
		// System.out.println(hashCode() + " de.ced.calculator.elements.Sum created! " +
		// Arrays.toString(elements));
	}

	@Override
	public double calculate() throws PCAnDieWand {
		double result = 0;
		for (Element element : elements) {
			result += element.calculate();
		}
		return result;
	}

	@Override
	public String symbol() {
		return null;
	}

	@Override
	public String info() {
		return null;
	}
}
