package de.ced.calculator;

import static de.ced.calculator.compiler.Debug.*;

public class Main {

	public static void main(String[] args) {
		if (args.length >= 1) {
			try {
				setDebugLevel(Integer.parseInt(args[0]));
			} catch (NumberFormatException e) {
				System.err.println("Invalid argument 0, has to be int.");
				System.exit(1);
			}
		}
		if (args.length >= 2 && "old".equals(args[1])) {
			new OldCalculator();
			return;
		}
		new Calculator();
	}
}
