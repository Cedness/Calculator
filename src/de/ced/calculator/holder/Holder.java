package de.ced.calculator.holder;

public interface Holder<T> {
	T value();
}
