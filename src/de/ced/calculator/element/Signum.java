package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Signum extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(v.signum());
	}

	@Override
	public String symbol() {
		return "sig";
	}

	@Override
	public String info() {
		return "Applies the signum-function to a number.";
	}
}
