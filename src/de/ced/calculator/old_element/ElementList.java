package de.ced.calculator.old_element;

import java.util.ArrayList;
import java.util.Arrays;

@Deprecated
public abstract class ElementList extends Element {

	protected final ArrayList<Element> elements;

	public ElementList(Element... elements) {
		this.elements = new ArrayList<>(Arrays.asList(elements));
	}

	public void addElements(Element... elements) {
		this.elements.addAll(Arrays.asList(elements));
		// System.out.println(hashCode() + " added: " + Arrays.toString(elements));
	}
}
