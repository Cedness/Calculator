package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Product extends Operator {

	@Override
	protected BigDecimal operate(BigDecimal a, BigDecimal b) throws PCAnDieWand {
		return a.multiply(b);
	}

	@Override
	public String symbol() {
		return "*";
	}

	@Override
	public String info() {
		return "Calculates the product of 2 operands.";
	}
}
