package de.ced.calculator.compiler.subcompiler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import de.ced.calculator.compiler.Symbols;
import de.ced.calculator.element.Difference;
import de.ced.calculator.element.Operator;
import de.ced.calculator.element.Sum;
import de.ced.calculator.holder.Holder;
import de.ced.calculator.holder.HolderFolder;
import de.ced.calculator.holder.NumberHolder;
import de.ced.calculator.holder.SymbolHolder;

public class SumCompiler extends OperatorCompiler {

	public SumCompiler(SubCompiler<List<Holder<?>>, ?> nextCompiler) {
		super(nextCompiler, false, true);
	}

	@Override
	protected Operator detectOperator(char symbol) {
		return switch (symbol) {
		case Symbols.PLUS -> new Sum();
		case Symbols.MINUS -> new Difference();
		default -> null;
		};
	}

	@Override
	protected boolean buildCustom(List<Holder<?>> holders, int i, Operator detected) {
		if (detected instanceof Difference) { // Detect inverting minus
			boolean invertingMinus = i == 0;
			if (!invertingMinus) {
				Holder<?> lastHolder = holders.get(i - 1);
				if (lastHolder instanceof SymbolHolder) {
					char lastSym = ((SymbolHolder) lastHolder).value();
					if (Symbols.OPERATORS.contains(lastSym)) { // If last symbol is an operator
						invertingMinus = true;
					}
				}
			}
			if (invertingMinus) {
				List<Holder<?>> sublist = holders.subList(i, holders.size()); // create sublist-view of holders
				List<Holder<?>> listCopy = new ArrayList<>(sublist);
				listCopy.add(0, new NumberHolder(new BigDecimal(0)));
				HolderFolder folder = new HolderFolder(listCopy, -1); // copy holders
				sublist.clear(); // remove orignal holders
				holders.add(i, folder); // replace inverting minus
				return true; // index ok due to backwards looping
			}
		}
		return false;
	}
}
