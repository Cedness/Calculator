package de.ced.calculator.compiler.subcompiler;

import static de.ced.calculator.compiler.Debug.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import de.ced.calculator.Element;
import de.ced.calculator.ErrorType;
import de.ced.calculator.PCAnDieWand;
import de.ced.calculator.compiler.Symbols;
import de.ced.calculator.holder.Holder;
import de.ced.calculator.holder.NumberHolder;
import de.ced.calculator.holder.SymbolHolder;

public class NumberCompiler extends SubCompiler<String, List<Holder<?>>> {

	public NumberCompiler(SubCompiler<List<Holder<?>>, ?> nextCompiler) {
		super(nextCompiler);
	}

	@Override
	public Element invoke(String equation) throws PCAnDieWand {
		// Check for illegal characters and isolate numbers
		if (debug(NUMBER_ISOLATION)) {
			System.out.println("Isolating numbers...");
		}
		char[] translatedEquation = equation.toCharArray();
		List<Holder<?>> holders = new ArrayList<>();
		int numStart = -1;
		for (int i = 0; i <= equation.length(); i++) {
			Character c = i == equation.length() ? ' ' : translatedEquation[i];
			if (Symbols.isNumberChar(c)) {
				if (numStart < 0) {
					if (debug(NUMBER_ISOLATION)) {
						System.out.println("found number start at " + i);
					}
					numStart = i;
				}
				continue;
			} else if (numStart >= 0) {
				if (debug(NUMBER_ISOLATION)) {
					System.out.println("found number end at " + i);
				}
				String rawNum = equation.substring(numStart, i);
				if (debug(NUMBER_ISOLATION)) {
					System.out.print("parsing " + rawNum + " to: ");
				}
				BigDecimal num;
				try {
					num = new BigDecimal(rawNum);
				} catch (NumberFormatException ex) {
					throw new PCAnDieWand(ErrorType.INPUT_ERROR, rawNum + " seems to be a number but is not valid.");
				}
				if (debug(NUMBER_ISOLATION)) {
					System.out.println(num);
				}
				holders.add(new NumberHolder(num));
				numStart = -1;
			}

			if (c == ' ') {
				continue;
			}
			if (!Symbols.SYMBOLS.contains(c)) {
				throw new PCAnDieWand(ErrorType.DEPRECATED, c + " is not a valid symbol.");
			}

			holders.add(new SymbolHolder(c));
		}
		if (debug(NUMBER_ISOLATION)) {
			System.out.println("Isolating numbers done.");
		}
		return invokeNext(holders);
	}
}
