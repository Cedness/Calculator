package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Power extends Operator {

	@Override
	protected BigDecimal operate(BigDecimal a, BigDecimal b) throws PCAnDieWand {
		return a.pow(b.intValue());
	}

	@Override
	public String symbol() {
		return "^";
	}

	@Override
	public String info() {
		return "Calculates the left operand to the power of the right operand.";
	}
}
