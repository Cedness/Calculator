package de.ced.calculator.old_element;

@Deprecated
public class Number extends Element {

	private final double value;

	public Number(double value) {
		this.value = value;
		// System.out.println(hashCode() + " de.ced.calculator.elements.Number
		// created!");
	}

	@Override
	public double calculate() {
		return value;
	}

	@Override
	public String symbol() {
		return null;
	}

	@Override
	public String info() {
		return null;
	}
}
