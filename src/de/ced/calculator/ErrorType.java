package de.ced.calculator;

public enum ErrorType {

	SYNTAX_ERROR("Syntax-Error"),
	INPUT_ERROR("Input-Error"),
	UNKNOWN("Unkown"),
	FATAL("Fatal"),
	@Deprecated
	DEPRECATED("Deprecated Error (Should no longer occurr, this is a bug!)");

	private final String name;

	ErrorType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
