package de.ced.calculator.compiler.subcompiler;

import static de.ced.calculator.compiler.Debug.*;

import java.util.List;

import de.ced.calculator.Element;
import de.ced.calculator.PCAnDieWand;
import de.ced.calculator.holder.Holder;

public abstract class MathCompiler extends SubCompiler<List<Holder<?>>, List<Holder<?>>> {

	protected static int compileRunId = 0;

	public static int getCompileRunId() {
		return compileRunId;
	}

	public static void reset() {
		compileRunId = 0;
	}

	public MathCompiler(SubCompiler<List<Holder<?>>, ?> nextCompiler) {
		super(nextCompiler);
	}

	protected void logInsertion(Element element, int compileRunId) {
		if (debug(COMPILE)) {
			System.out.println("[" + compileRunId + "]" + getClass().getSimpleName() + ": Inserted: " + element);
		}
	}

	@Override
	public final Element invoke(List<Holder<?>> holders) throws PCAnDieWand {
		int compileRunId = MathCompiler.compileRunId++;
		if (debug(COMPILE, VERBOSE)) {
			System.out.println("[" + compileRunId + "]" + getClass().getSimpleName() + ": Starting...");
		}
		Element element = build(holders, compileRunId);
		holders.clear();
		if (debug(COMPILE)) {
			System.out.println("[" + compileRunId + "]" + getClass().getSimpleName() + ": Done. Built: " + element);
		}
		return element;
	}

	public abstract Element build(List<Holder<?>> holders, int compileRunId) throws PCAnDieWand;
}
