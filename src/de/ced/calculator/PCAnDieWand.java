package de.ced.calculator;

public class PCAnDieWand extends Exception {

	private static final long serialVersionUID = 1L;
	private final ErrorType errorType;
	private final String info;

	public PCAnDieWand(ErrorType errorType) {
		this(errorType, null);
	}

	public PCAnDieWand(ErrorType errorType, String info) {
		super(errorType.getName() + (info == null ? "" : ": " + info));
		this.errorType = errorType;
		this.info = info;
	}

	public ErrorType getErrorType() {
		return errorType;
	}

	public String getInfo() {
		return info;
	}
}
