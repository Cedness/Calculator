package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class GenericRoot extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		v = v.subtract(new BigDecimal(1));
		int i = v.remainder(new BigDecimal(9)).intValue();
		return new BigDecimal(i + 1);
	}

	@Override
	public String symbol() {
		return "grt";
	}

	@Override
	public String info() {
		return "Calculates the generic root of a number.";
	}
}
