package de.ced.calculator.compiler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
public class Symbols {

	public static final char SEPARATOR = '#';
	public static final String TRIPLE_SEPARATOR = String.valueOf(new char[] { SEPARATOR, SEPARATOR, SEPARATOR });
	public static final List<Map<Character, Object>> ALIASES = new ArrayList<>();

	static {
		BufferedReader reader = new BufferedReader(new InputStreamReader(Symbols.class.getResourceAsStream("symbols.txt")));
		try {
			Map<Character, Object> rootMap = null;
			for (String line; reader.ready();) {
				line = reader.readLine();
				if (line.contains(":" + TRIPLE_SEPARATOR)) {
					rootMap = null;
				}
				if (line.contains(TRIPLE_SEPARATOR + ":")) {
					// System.out.println("Add root map");
					rootMap = new HashMap<>();
					Map<Character, Object> spaceEliminator = new HashMap<>();
					spaceEliminator.put('#', "");
					rootMap.put(' ', spaceEliminator);
					ALIASES.add(rootMap);
				}
				if (rootMap != null && !line.contains(TRIPLE_SEPARATOR)) {
					String[] keyValuePair = line.split(String.valueOf(SEPARATOR));
					if (keyValuePair.length != 2) {
						continue;
					}
					Map<Character, Object> currentMap = rootMap;
					// System.out.print("Translate: ");
					for (Character c : keyValuePair[0].toCharArray()) {
						// System.out.print(c);
						Map<Character, Object> subMap;
						if (currentMap.containsKey(c)) {
							subMap = (Map<Character, Object>) currentMap.get(c);
						} else {
							currentMap.put(c, subMap = new HashMap<>());
						}
						currentMap = subMap;
					}
					currentMap.put(SEPARATOR, keyValuePair[1]);
					// System.out.println(": " + keyValuePair[1]);
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static final char COMMA = '.';

	public static final boolean isNumberChar(char c) {
		return "0123456789".indexOf(c) >= 0 || c == COMMA;
	}

	public static final char PLUS = '+';
	public static final char MINUS = '-';

	public static final char MUL = '*';
	public static final char DIV = '/';
	public static final char MOD = '%';

	public static final char POW = '^';

	public static final char LOG = 'l';

	public static final char SQRT = 'q';
	public static final char CBRT = 'Q';
	public static final char GRT = 'g';

	public static final char SIG = 'G';

	public static final char SIN = 's';
	public static final char COS = 'c';
	public static final char TAN = 't';
	public static final char ASIN = 'S';
	public static final char ACOS = 'C';
	public static final char ATAN = 'T';

	public static final char ABS = 'a';
	public static final char ROUND = 'k';
	public static final char CEIL = 'R';
	public static final char FLOOR = 'r';

	public static final char DEG = 'd';
	public static final char RAD = 'D';

	public static final char HELP = 'h';

	public static final char BR_OPEN = '(';
	public static final char BR_CLOSE = ')';

	public static final char EMPTY = '\\';

	public static final List<Character> LOW_OPERATORS = Arrays.asList(new Character[] {
			PLUS,
			MINUS
	});
	public static final List<Character> MEDIOCRE_OPERATORS = Arrays.asList(new Character[] {
			MUL,
			DIV,
			MOD
	});
	public static final List<Character> HIGH_OPERATORS = Arrays.asList(new Character[] {
			POW
	});
	public static final List<Character> OPERATORS = Arrays.asList(new Character[] {
			PLUS,
			MINUS,

			MUL,
			DIV,
			MOD,

			POW
	});

	public static final List<Character> BRACKETS = Arrays.asList(new Character[] {
			BR_OPEN,
			BR_CLOSE
	});

	public static final List<Character> FUNCTIONS = Arrays.asList(new Character[] {
			LOG,

			SQRT,
			CBRT,
			GRT,

			SIG,

			SIN,
			COS,
			TAN,
			ASIN,
			ACOS,
			ATAN,

			ABS,
			ROUND,
			CEIL,
			FLOOR,

			DEG,
			RAD,

			HELP
	});

	public static final List<Character> SYMBOLS = Arrays.asList(new Character[] {
			PLUS,
			MINUS,

			MUL,
			DIV,
			MOD,

			POW,

			LOG,

			SQRT,
			CBRT,
			GRT,

			SIG,

			SIN,
			COS,
			TAN,
			ASIN,
			ACOS,
			ATAN,

			ABS,
			ROUND,
			CEIL,
			FLOOR,

			DEG,
			RAD,

			HELP,

			BR_OPEN,
			BR_CLOSE,

			EMPTY
	});
}
