package de.ced.calculator.element;

import static de.ced.calculator.compiler.Debug.*;

import java.math.BigDecimal;

import de.ced.calculator.Element;
import de.ced.calculator.PCAnDieWand;

public abstract class OneparamFunction extends Function {

	public Element getE() {
		return getE(0);
	}

	public void setE(Element e) {
		setEs(e);
	}

	@Override
	public BigDecimal calculate() throws PCAnDieWand {
		BigDecimal param = e[0].calculate();
		BigDecimal v = apply(param);
		if (debug(CALCULATION)) {
			System.out.println(symbol() + enbracket(String.valueOf(param)) + "=" + v);
		}
		return v;
	}

	@Override
	protected BigDecimal apply(BigDecimal... v) throws PCAnDieWand {
		return apply(v[0]);
	}

	protected abstract BigDecimal apply(BigDecimal v) throws PCAnDieWand;

	@Override
	public String toString() {
		return symbol() + enbracket(getE());
	}
}
