package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Root extends Function {

	@Override
	protected BigDecimal apply(BigDecimal... v) throws PCAnDieWand {
		return new BigDecimal(0);
	}

	@Override
	public String symbol() {
		return "root";
	}

	@Override
	public String info() {
		return "Calculates the second-operandth root of the first operand.";
	}
}
