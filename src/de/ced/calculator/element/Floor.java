package de.ced.calculator.element;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import de.ced.calculator.PCAnDieWand;

public class Floor extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return v.round(new MathContext(0, RoundingMode.DOWN));
	}

	@Override
	public String symbol() {
		return "floor";
	}

	@Override
	public String info() {
		return "Rounds the number down to the next int-value.";
	}
}
