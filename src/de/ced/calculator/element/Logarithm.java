package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Logarithm extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.log(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "log";
	}

	@Override
	public String info() {
		return "Calculates the natural logarithm of a number.";
	}
}
