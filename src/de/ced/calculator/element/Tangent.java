package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Tangent extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.tan(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "tan";
	}

	@Override
	public String info() {
		return "Calculates the tangent of a number.";
	}
}
