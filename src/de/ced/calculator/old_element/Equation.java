package de.ced.calculator.old_element;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.ced.calculator.ErrorType;
import de.ced.calculator.PCAnDieWand;

@Deprecated
public class Equation extends Element {

	private static final List<Character> ALLOWED_CHARS = Arrays.asList(
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '+', '-', '*', '/', '(', ')');

	private static final List<Character> NUMBER_CHARS = Arrays.asList(
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.');

	private boolean isNumberChar(char character) {
		return NUMBER_CHARS.contains(character);
	}

	private boolean isSumChar(char character) {
		return character == '+' || character == '-';
	}

	private boolean isProductChar(char character) {
		return character == '*' || character == '/';
	}

	private boolean isMinusChar(char character) {
		return character == '-';
	}

	private boolean isDivideChar(char character) {
		return character == '/';
	}

	private boolean isOpeningBraceChar(char character) {
		return character == '(';
	}

	private boolean isClosingBraceChar(char character) {
		return character == ')';
	}

	private final String rawEquation;

	public Equation(String rawEquation) {
		this.rawEquation = rawEquation;
	}

	@Override
	public double calculate() throws PCAnDieWand {
		return buildLayer(prepareString(rawEquation)).calculate();
	}

	private Element buildLayer(char[] rawLayer) throws PCAnDieWand {
		/*
		 * for (char c : rawLayer) {
		 * System.out.print(c);
		 * }
		 * System.out.println();
		 */

		Sum sum = new Sum();

		Element currentElement = null;
		boolean negative = false;

		boolean divided = false;

		StringBuilder builder = null;

		List<Character> subLayer = null;
		int bracesOpen = 0;

		for (int currentIndex = 0; currentIndex <= rawLayer.length; currentIndex++) {
			char character = currentIndex == rawLayer.length ? '+' : rawLayer[currentIndex];
			// System.out.println(character);

			if (builder != null) {

				if (!isNumberChar(character)) {
					double value = (negative ? -1 : 1) * Double.parseDouble(builder.toString());
					if (divided) {
						value = 1 / value;
					}
					Number number = new Number(value);
					builder = null;
					negative = false;
					divided = false;
					if (currentElement instanceof ElementList) {
						((ElementList) currentElement).addElements(number);
					} else {
						currentElement = number;
					}
					currentIndex--;
					continue;
				}

				builder.append(character);
				continue;

			}

			if (subLayer != null) {

				if (isClosingBraceChar(character)) {
					bracesOpen--;

					if (bracesOpen == 0) {
						Element element = buildLayer(listToArray(subLayer));
						if (negative) {
							element = new Product(element, new Number(-1));
						}
						if (divided) {
							element = new Quotient(new Number(1), element);
						}
						subLayer = null;
						negative = false;
						divided = false;
						if (currentElement instanceof ElementList) {
							((ElementList) currentElement).addElements(element);
						} else {
							currentElement = element;
						}
						continue;
					}
				}

				if (isOpeningBraceChar(character)) {
					bracesOpen++;
				}

				subLayer.add(character);
				continue;
			}

			if (currentElement != null && isSumChar(character)) {
				sum.addElements(currentElement);
				currentElement = null;
			}

			// Finish summant
			if (isSumChar(character)) {
				negative = isMinusChar(character);
				continue;
			}

			if (isNumberChar(character)) {
				builder = new StringBuilder();
				currentIndex--;
				continue;
			}

			if (isOpeningBraceChar(character)) {
				subLayer = new ArrayList<>();
				bracesOpen++;
				continue;
			}

			if (currentElement == null) {
				throw new PCAnDieWand(ErrorType.UNKNOWN);
			}

			if (isProductChar(character)) {
				currentElement = new Product(currentElement);
				divided = isDivideChar(character);
			}
		}

		if (negative || divided || builder != null || subLayer != null) {
			throw new PCAnDieWand(ErrorType.UNKNOWN);
		}

		if (currentElement != null) {
			sum.addElements(currentElement);
		}

		return sum;
	}

	private char[] prepareString(String rawInput) {
		final char[] input = rawInput.replaceAll(",", ".").toCharArray();
		List<Character> rawOutput = new ArrayList<>();

		for (char character : input) {
			if (ALLOWED_CHARS.contains(character)) {
				rawOutput.add(character);
			}
		}
		return listToArray(rawOutput);
	}

	private char[] listToArray(List<Character> input) {
		char[] output = new char[input.size()];
		for (int i = 0; i < input.size(); i++) {
			output[i] = input.get(i);
		}
		return output;

	}

	@Override
	public String symbol() {
		return null;
	}

	@Override
	public String info() {
		return null;
	}
}
