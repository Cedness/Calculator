package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Arcsinus extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.asin(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "asin";
	}

	@Override
	public String info() {
		return "Calculates the arcsinus of a number.";
	}
}
