package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.Element;
import de.ced.calculator.PCAnDieWand;

public class Numberio extends Element {

	private final BigDecimal v;

	public Numberio(BigDecimal v) {
		this.v = v;
	}

	@Override
	public BigDecimal calculate() throws PCAnDieWand {
		return v;
	}

	@Override
	public String symbol() {
		return v.toString();
	}

	@Override
	public String toString() {
		return symbol();
	}

	@Override
	public String info() {
		return "This is a number.";
	}
}
