package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Sinus extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.sin(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "sin";
	}

	@Override
	public String info() {
		return "Calculates the sinus of a number.";
	}
}
