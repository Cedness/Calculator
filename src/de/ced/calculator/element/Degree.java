package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Degree extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.toDegrees(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "deg";
	}

	@Override
	public String info() {
		return "Turns radians into degrees";
	}
}
