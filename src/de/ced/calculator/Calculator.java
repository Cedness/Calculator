package de.ced.calculator;

import static de.ced.calculator.compiler.Debug.*;

import java.math.BigDecimal;
import java.util.Scanner;

import de.ced.calculator.compiler.EquationCompiler;

public class Calculator {

	public Calculator() {
		System.out.println("======================");
		System.out.println("| SadCalculator v2.1 |");
		System.out.println("======================");
		if (debug()) {
			System.out.println("Debug-Output set to level " + Integer.toHexString(getDebugLevel()));
		}
		if (debug(COMPILE | BUILD | CALCULATION | EQUATION)) {
			System.out.println("Info: Natural order of operations is internally represented only by using brackets.");
		}

		Scanner scanner = new Scanner(System.in);

		EquationCompiler equationCompiler = new EquationCompiler();

		while (true) {

			// INPUT
			System.out.print(">> ");
			String input = scanner.nextLine();
			if (input.equals("exit")) {
				break;
			}

			try {

				// PROCESS
				Element equation = equationCompiler.build(input);
				if (debug(CALCULATION)) {
					System.out.println("====================");
					System.out.println("Calculating equation...");
				}
				BigDecimal result = equation.calculate();

				// OUTPUT
				if (debug(EQUATION)) {
					System.out.println("====================");
					System.out.println("Result:");
					System.out.println(equation + "=" + result);
				} else {
					System.out.println(result);
				}

				// ERROR HANDLING
			} catch (PCAnDieWand e) {
				System.out.println(e.getMessage());
			}
		}
		scanner.close();
	}
}
