package de.ced.calculator.old_element;

import de.ced.calculator.PCAnDieWand;

public abstract class Element {

	public abstract double calculate() throws PCAnDieWand;

	public abstract String symbol();

	public abstract String info();
}
