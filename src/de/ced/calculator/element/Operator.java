package de.ced.calculator.element;

import static de.ced.calculator.compiler.Debug.*;

import java.math.BigDecimal;

import de.ced.calculator.Element;
import de.ced.calculator.PCAnDieWand;

public abstract class Operator extends Element {

	protected Element a;
	protected Element b;

	public Element getA() {
		return a;
	}

	public void setA(Element a) {
		this.a = a;
	}

	public Element getB() {
		return b;
	}

	public void setB(Element b) {
		this.b = b;
	}

	@Override
	public final BigDecimal calculate() throws PCAnDieWand {
		BigDecimal a = this.a.calculate();
		BigDecimal b = this.b.calculate();
		BigDecimal v = operate(a, b);
		if (debug(CALCULATION)) {
			System.out.println(a + symbol() + b + "=" + v);
		}
		return v;
	}

	protected abstract BigDecimal operate(BigDecimal a, BigDecimal b) throws PCAnDieWand;

	@Override
	public String toString() {
		return enbracketIfNecassary(a) + symbol() + enbracketIfNecassary(b);
	}
}
