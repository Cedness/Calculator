package de.ced.calculator.element;

import static de.ced.calculator.compiler.Debug.*;

import java.math.BigDecimal;

import de.ced.calculator.Element;
import de.ced.calculator.PCAnDieWand;

public abstract class Function extends Element {

	protected Element[] e;

	public Element getE(int i) {
		return e[i];
	}

	public Element[] getEs() {
		return e;
	}

	public void setEs(Element... e) {
		this.e = e;
	}

	@Override
	public BigDecimal calculate() throws PCAnDieWand {
		BigDecimal[] params = new BigDecimal[e.length];
		for (int i = 0; i < e.length; i++) {
			params[i] = e[i].calculate();
		}
		BigDecimal v = apply(params);
		if (debug(CALCULATION)) {
			StringBuilder builder = new StringBuilder();
			int i = 0;
			if (params.length > 0) {
				while (true) {
					builder.append(params[i++]);
					if (i >= params.length) {
						break;
					}
					builder.append(", ");
				}
			}
			System.out.println(symbol() + enbracket(builder.toString()) + "=" + v);
		}
		return v;
	}

	protected abstract BigDecimal apply(BigDecimal... v) throws PCAnDieWand;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		int i = 0;
		if (e.length > 0) {
			while (true) {
				builder.append(e[i++]);
				if (i >= e.length) {
					break;
				}
				builder.append(", ");
			}
		}
		return symbol() + enbracket(builder.toString());
	}
}
