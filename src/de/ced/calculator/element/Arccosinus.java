package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Arccosinus extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.acos(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "acos";
	}

	@Override
	public String info() {
		return "Calculates the arccosinus of a number.";
	}
}
