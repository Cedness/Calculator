package de.ced.calculator.compiler.subcompiler;

import java.util.List;

import de.ced.calculator.Element;
import de.ced.calculator.ErrorType;
import de.ced.calculator.PCAnDieWand;
import de.ced.calculator.compiler.Symbols;
import de.ced.calculator.element.Absolute;
import de.ced.calculator.element.Arccosinus;
import de.ced.calculator.element.Arcsinus;
import de.ced.calculator.element.Arctangent;
import de.ced.calculator.element.Ceil;
import de.ced.calculator.element.Cosinus;
import de.ced.calculator.element.CubeRoot;
import de.ced.calculator.element.Degree;
import de.ced.calculator.element.Floor;
import de.ced.calculator.element.Function;
import de.ced.calculator.element.GenericRoot;
import de.ced.calculator.element.Help;
import de.ced.calculator.element.Logarithm;
import de.ced.calculator.element.Numberio;
import de.ced.calculator.element.Radian;
import de.ced.calculator.element.Round;
import de.ced.calculator.element.Signum;
import de.ced.calculator.element.Sinus;
import de.ced.calculator.element.SquareRoot;
import de.ced.calculator.element.Tangent;
import de.ced.calculator.holder.Holder;
import de.ced.calculator.holder.HolderFolder;
import de.ced.calculator.holder.NumberHolder;
import de.ced.calculator.holder.SymbolHolder;

public class FunctionCompiler extends MathCompiler {

	public FunctionCompiler(SubCompiler<List<Holder<?>>, ?> nextCompiler) {
		super(nextCompiler);
	}

	@Override
	public Element build(List<Holder<?>> holders, int compileRunId) throws PCAnDieWand {
		if (holders.isEmpty()) {
			throw new PCAnDieWand(ErrorType.SYNTAX_ERROR, "Value missing.");
		} else if (holders.size() > 2) {
			throw new PCAnDieWand(ErrorType.SYNTAX_ERROR);
		}
		Holder<?> value = holders.get(holders.size() - 1);
		if (!(value instanceof NumberHolder || value instanceof HolderFolder)) {
			throw new PCAnDieWand(ErrorType.SYNTAX_ERROR);
		}
		Element element;
		if (value instanceof HolderFolder) {
			element = invokeNext(((HolderFolder) value).value());
		} else {
			element = new Numberio(((NumberHolder) value).value());
		}
		logInsertion(element, compileRunId);
		if (holders.size() == 2) {
			Holder<?> function = holders.get(0);
			if (!(function instanceof SymbolHolder)) {
				throw new PCAnDieWand(ErrorType.SYNTAX_ERROR);
			}
			Function detection = detectFunction(((SymbolHolder) function).value());
			detection.setEs(element);
			element = detection;
		}
		return element;
	}

	private Function detectFunction(char symbol) throws PCAnDieWand {
		return switch (symbol) {
		case Symbols.LOG -> new Logarithm();
		case Symbols.SQRT -> new SquareRoot();
		case Symbols.CBRT -> new CubeRoot();
		case Symbols.GRT -> new GenericRoot();
		case Symbols.SIG -> new Signum();
		case Symbols.SIN -> new Sinus();
		case Symbols.COS -> new Cosinus();
		case Symbols.TAN -> new Tangent();
		case Symbols.ASIN -> new Arcsinus();
		case Symbols.ACOS -> new Arccosinus();
		case Symbols.ATAN -> new Arctangent();
		case Symbols.ABS -> new Absolute();
		case Symbols.ROUND -> new Round();
		case Symbols.CEIL -> new Ceil();
		case Symbols.FLOOR -> new Floor();
		case Symbols.DEG -> new Degree();
		case Symbols.RAD -> new Radian();
		case Symbols.HELP -> new Help();
		default -> throw new PCAnDieWand(ErrorType.SYNTAX_ERROR);
		};
	}
}
