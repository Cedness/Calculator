package de.ced.calculator;

import static de.ced.calculator.compiler.Debug.*;

import java.util.Scanner;

import de.ced.calculator.old_element.Equation;

public class OldCalculator {

	public OldCalculator() {
		System.out.println("======================");
		System.out.println("| SadCalculator v1.0 |");
		System.out.println("======================");
		if (debug()) {
			System.out.println("Debug-Output set to level " + Integer.toHexString(getDebugLevel()));
		}

		Scanner scanner = new Scanner(System.in);

		while (true) {

			// INPUT
			System.out.print(">> ");
			String input = scanner.nextLine();
			if (input.equals("exit")) {
				break;
			}

			try {

				// PROCESS
				Equation equation = new Equation(input);
				double output = equation.calculate();

				// OUTPUT
				if (output != (int) output) {
					System.out.println(output);
				} else {
					int roundOutput = (int) output;
					System.out.println(roundOutput);
				}

				// ERROR HANDLING
			} catch (PCAnDieWand e) {
				System.out.println(e.getMessage());
			}
		}
		scanner.close();
	}
}
