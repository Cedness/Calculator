package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Radian extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.toRadians(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "rad";
	}

	@Override
	public String info() {
		return "Turns degrees into radians.";
	}
}
