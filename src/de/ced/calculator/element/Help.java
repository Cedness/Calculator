package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.Element;
import de.ced.calculator.PCAnDieWand;

public class Help extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		Element e = getE();
		System.out.println("\"" + e.symbol() + "\": " + e.info());
		return v;
	}

	@Override
	public String symbol() {
		return "help";
	}

	@Override
	public String info() {
		return "Prints help about given function or operation. (Values are passed through.)";
	}
}
