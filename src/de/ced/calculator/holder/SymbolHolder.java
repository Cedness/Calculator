package de.ced.calculator.holder;

public class SymbolHolder implements Holder<Character> {
	private final char value;

	public SymbolHolder(char value) {
		this.value = value;
	}

	@Override
	public Character value() {
		return value;
	}
}
