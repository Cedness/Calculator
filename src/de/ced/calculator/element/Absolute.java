package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Absolute extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return v.abs();
	}

	@Override
	public String symbol() {
		return "abs";
	}

	@Override
	public String info() {
		return "Calculates the absolute value of a number.";
	}
}
