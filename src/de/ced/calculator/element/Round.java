package de.ced.calculator.element;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import de.ced.calculator.PCAnDieWand;

public class Round extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return v.round(new MathContext(0, RoundingMode.HALF_UP));
	}

	@Override
	public String symbol() {
		return "round";
	}

	@Override
	public String info() {
		return "Rounds the number to the next int-value with ties being round up.";
	}
}
