package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Quotient extends Operator {

	@Override
	protected BigDecimal operate(BigDecimal a, BigDecimal b) throws PCAnDieWand {
		return a.divide(b);
	}

	@Override
	public String symbol() {
		return "/";
	}

	@Override
	public String info() {
		return "Calculates the quotient of 2 operands.";
	}
}
