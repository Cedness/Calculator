package de.ced.calculator.compiler.subcompiler;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import de.ced.calculator.Element;
import de.ced.calculator.ErrorType;
import de.ced.calculator.PCAnDieWand;
import de.ced.calculator.compiler.Symbols;
import de.ced.calculator.holder.Holder;
import de.ced.calculator.holder.HolderFolder;
import de.ced.calculator.holder.SymbolHolder;

public class LayerCompiler extends SubCompiler<List<Holder<?>>, List<Holder<?>>> {

	private static int maxDepth = 0;

	public static int getMaxDepth() {
		return maxDepth;
	}

	public LayerCompiler(SubCompiler<List<Holder<?>>, ?> nextCompiler) {
		super(nextCompiler);
	}

	@Override
	public Element invoke(List<Holder<?>> holders) throws PCAnDieWand {
		Stack<Integer> starts = new Stack<>();
		maxDepth = 0;
		for (int i = 0; i < holders.size(); i++) {
			Holder<?> holder = holders.get(i);
			if (!(holder instanceof SymbolHolder)) {
				continue;
			}
			char sym = ((SymbolHolder) holder).value();
			if (sym == Symbols.BR_OPEN) {
				starts.push(i); // save position of opening bracket on stack
				if (starts.size() > maxDepth) { // increase maxDepth for statistics
					maxDepth++;
				}
				continue;
			} else if (sym == Symbols.BR_CLOSE) {
				int depth = starts.size(); // acquire current depth
				if (depth <= 0) { // throw if more closing brackets than opening brackets
					throw new PCAnDieWand(ErrorType.SYNTAX_ERROR, "No matching count of opening and closing brackets.");
				}
				int start = starts.pop(); // acquire matching opening bracket
				List<Holder<?>> sublist = holders.subList(start + 1, i); // create sublist-view of holders
				HolderFolder folder = new HolderFolder(new ArrayList<>(sublist), depth); // copy holders
				sublist.clear(); // remove orignal holders
				holders.remove(start); // remove opening bracket
				holders.set(start, folder); // replace closing bracket
				i = start;
			}
		}
		if (starts.size() > 0) { // throw if more opening brackets than closing brackets
			throw new PCAnDieWand(ErrorType.SYNTAX_ERROR, "No matching count of opening and closing brackets.");
		}
		return invokeNext(holders);
	}
}
