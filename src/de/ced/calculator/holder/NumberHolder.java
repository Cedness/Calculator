package de.ced.calculator.holder;

import java.math.BigDecimal;

public class NumberHolder implements Holder<BigDecimal> {
	private final BigDecimal value;

	public NumberHolder(BigDecimal value) {
		this.value = value;
	}

	@Override
	public BigDecimal value() {
		return value;
	}
}
