package de.ced.calculator.holder;

import java.util.List;

public class HolderFolder implements Holder<List<Holder<?>>> {
	private final List<Holder<?>> folder;
	private final int depth;

	public HolderFolder(List<Holder<?>> folder, int depth) {
		this.folder = folder;
		this.depth = depth;
	}

	@Override
	public List<Holder<?>> value() {
		return folder;
	}

	public int getDepth() {
		return depth;
	}
}
