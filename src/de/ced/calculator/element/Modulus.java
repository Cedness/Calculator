package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Modulus extends Operator {

	@Override
	protected BigDecimal operate(BigDecimal a, BigDecimal b) throws PCAnDieWand {
		return a.remainder(b);
	}

	@Override
	public String symbol() {
		return "%";
	}

	@Override
	public String info() {
		return "Calculates the modulus of a division between the operands.";
	}
}
