package de.ced.calculator.compiler.subcompiler;

import java.util.List;

import de.ced.calculator.Element;
import de.ced.calculator.ErrorType;
import de.ced.calculator.PCAnDieWand;
import de.ced.calculator.element.Operator;
import de.ced.calculator.holder.Holder;
import de.ced.calculator.holder.SymbolHolder;

public abstract class OperatorCompiler extends MathCompiler {

	private final boolean rightToLeftPredescence;
	private final boolean operatorAtBeginningAllowed;

	public OperatorCompiler(SubCompiler<List<Holder<?>>, ?> nextCompiler, boolean rightToLeftPredescence, boolean operatorAtBeginningAllowed) {
		super(nextCompiler);
		this.rightToLeftPredescence = rightToLeftPredescence;
		this.operatorAtBeginningAllowed = operatorAtBeginningAllowed;
	}

	@Override
	public Element build(List<Holder<?>> holders, int compileRunId) throws PCAnDieWand {
		Operator root = null;
		Operator current = null;
		for (int i = holders.size() - 1; i >= 0; i--) {
			Holder<?> holder = holders.get(i);
			if (!(holder instanceof SymbolHolder)) {
				continue;
			}
			Operator detected = detectOperator(((SymbolHolder) holder).value());
			if (detected == null) {
				continue;
			}
			if (i == holders.size() - 1 || !operatorAtBeginningAllowed && i == 0) {
				throw new PCAnDieWand(ErrorType.SYNTAX_ERROR, "Illegal usage of operator " + detected.symbol());
			}
			if (buildCustom(holders, i, detected)) {
				continue;
			}

			Element element = invokeNext(holders.subList(i + 1, holders.size()));
			logInsertion(element, compileRunId);
			holders.remove(i);
			Operator previous = current;
			current = detected;
			current.setB(element);
			if (rightToLeftPredescence) {
				root = current;
				if (previous != null) {
					current.setB(previous);
					previous.setA(element);
				}
			} else {
				if (root == null) {
					root = current;
				}
				if (previous != null) {
					previous.setA(current);
				}
			}
		}
		Element element = invokeNext(holders);
		if (root != null) {
			current.setA(element);
			element = root;
		}
		return element;
	}

	protected abstract Operator detectOperator(char symbol);

	protected boolean buildCustom(List<Holder<?>> holders, int i, Operator detected) {
		return false;
	}
}
