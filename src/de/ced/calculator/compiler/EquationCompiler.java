package de.ced.calculator.compiler;

import static de.ced.calculator.compiler.Debug.*;

import de.ced.calculator.Element;
import de.ced.calculator.PCAnDieWand;
import de.ced.calculator.compiler.subcompiler.FunctionCompiler;
import de.ced.calculator.compiler.subcompiler.LayerCompiler;
import de.ced.calculator.compiler.subcompiler.MathCompiler;
import de.ced.calculator.compiler.subcompiler.NumberCompiler;
import de.ced.calculator.compiler.subcompiler.PowerCompiler;
import de.ced.calculator.compiler.subcompiler.PreCompiler;
import de.ced.calculator.compiler.subcompiler.ProductCompiler;
import de.ced.calculator.compiler.subcompiler.SubCompiler;
import de.ced.calculator.compiler.subcompiler.SumCompiler;

public class EquationCompiler {

	private final SubCompiler<String, ?> compilerChain;

	public EquationCompiler() {
		MathCompiler lastCompiler;
		MathCompiler recursiveCompiler;
		compilerChain = new PreCompiler(
				new NumberCompiler(
						new LayerCompiler(
								recursiveCompiler = new SumCompiler(
										new ProductCompiler(
												new PowerCompiler(
														lastCompiler = new FunctionCompiler(null)))))));
		lastCompiler.setNextCompiler(recursiveCompiler);
	}

	public Element build(String rawEquation) throws PCAnDieWand {
		if (debug(BUILD)) {
			System.out.println("Building equation from " + rawEquation);
		}

		MathCompiler.reset();
		Element element = compilerChain.invoke(rawEquation);

		if (debug(BUILD)) {
			System.out.println("Used " + MathCompiler.getCompileRunId() + " compiler runs.");
			System.out.println("Equation completed:");
			System.out.println(element);
		}
		return element;
	}
}
