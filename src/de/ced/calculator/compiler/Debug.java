package de.ced.calculator.compiler;

public class Debug {
	private static int level = 0;
	public static final int VERBOSE = 0x1;
	public static final int EQUATION = 0x2;
	public static final int CALCULATION = 0x4;
	public static final int BUILD = 0x8;
	public static final int COMPILE = 0x10;
	public static final int NUMBER_ISOLATION = 0x20;
	public static final int REPLACE = 0x40;

	public static int getDebugLevel() {
		return level;
	}

	public static void setDebugLevel(int level) {
		Debug.level = level;
	}

	public static void setDebug(boolean enableAll) {
		level = enableAll ? -1 : 0;
	}

	public static boolean debug() {
		return level != 0;
	}

	public static boolean debug(int debugMode) {
		return (level & debugMode) != 0;
	}

	public static boolean debug(int... debugModes) {
		for (int debugMode : debugModes) {
			if (!debug(debugMode)) {
				return false;
			}
		}
		return true;
	}
}
