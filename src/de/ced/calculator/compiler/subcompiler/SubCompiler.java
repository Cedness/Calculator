package de.ced.calculator.compiler.subcompiler;

import de.ced.calculator.Element;
import de.ced.calculator.PCAnDieWand;

public abstract class SubCompiler<I, O> {

	protected SubCompiler<O, ?> nextCompiler;

	public SubCompiler(SubCompiler<O, ?> nextCompiler) {
		setNextCompiler(nextCompiler);
	}

	public SubCompiler<O, ?> getNextCompiler() {
		return nextCompiler;
	}

	public void setNextCompiler(SubCompiler<O, ?> nextCompiler) {
		this.nextCompiler = nextCompiler;
	}

	public abstract Element invoke(I input) throws PCAnDieWand;

	protected final Element invokeNext(O output) throws PCAnDieWand {
		return nextCompiler.invoke(output);
	}
}
