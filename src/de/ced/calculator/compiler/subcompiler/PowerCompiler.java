package de.ced.calculator.compiler.subcompiler;

import java.util.List;

import de.ced.calculator.compiler.Symbols;
import de.ced.calculator.element.Operator;
import de.ced.calculator.element.Power;
import de.ced.calculator.holder.Holder;

public class PowerCompiler extends OperatorCompiler {

	public PowerCompiler(SubCompiler<List<Holder<?>>, ?> nextCompiler) {
		super(nextCompiler, true, false);
	}

	@Override
	protected Operator detectOperator(char symbol) {
		return switch (symbol) {
		case Symbols.POW -> new Power();
		default -> null;
		};
	}
}
