package de.ced.calculator.compiler.subcompiler;

import java.util.List;

import de.ced.calculator.compiler.Symbols;
import de.ced.calculator.element.Modulus;
import de.ced.calculator.element.Operator;
import de.ced.calculator.element.Product;
import de.ced.calculator.element.Quotient;
import de.ced.calculator.holder.Holder;

public class ProductCompiler extends OperatorCompiler {

	public ProductCompiler(SubCompiler<List<Holder<?>>, ?> nextCompiler) {
		super(nextCompiler, false, false);
	}

	@Override
	protected Operator detectOperator(char symbol) {
		return switch (symbol) {
		case Symbols.MUL -> new Product();
		case Symbols.DIV -> new Quotient();
		case Symbols.MOD -> new Modulus();
		default -> null;
		};
	}

	@Override
	protected boolean buildCustom(List<Holder<?>> holders, int i, Operator detected) {
		// TODO detect invisible *
		return false;
	}
}
