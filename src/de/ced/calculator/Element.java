package de.ced.calculator;

import java.math.BigDecimal;

import de.ced.calculator.compiler.Symbols;
import de.ced.calculator.element.Operator;

public abstract class Element {

	public abstract BigDecimal calculate() throws PCAnDieWand;

	public abstract String symbol();

	public abstract String info();

	@Override
	public String toString() {
		return enbracket(symbol());
	}

	protected String enbracketIfNecassary(Element element) {
		return element instanceof Operator ? enbracket(element) : element.toString();
	}

	protected String enbracket(Element element) {
		return enbracket(element.toString());
	}

	protected String enbracket(String expression) {
		return Symbols.BR_OPEN + expression + Symbols.BR_CLOSE;
	}
}
