package de.ced.calculator.compiler.subcompiler;

import static de.ced.calculator.compiler.Debug.*;

import java.util.Map;

import de.ced.calculator.Element;
import de.ced.calculator.ErrorType;
import de.ced.calculator.PCAnDieWand;
import de.ced.calculator.compiler.Symbols;

public class PreCompiler extends SubCompiler<String, String> {

	public PreCompiler(SubCompiler<String, ?> nextCompiler) {
		super(nextCompiler);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Element invoke(String rawEquation) throws PCAnDieWand {
		if (debug(REPLACE)) {
			System.out.println("Replacing...");
		}
		for (Map<Character, Object> rootMap : Symbols.ALIASES) {
			char[] rawEquationArray = rawEquation.toCharArray();

			StringBuilder builder = new StringBuilder();

			Map<Character, Object> currentMap = rootMap;
			for (int i = 0; i <= rawEquationArray.length; i++) {
				char c = i == rawEquationArray.length ? ' ' : rawEquationArray[i];
				if (currentMap.containsKey(c)) {
					if (debug(REPLACE)) {
						System.out.println("found potential values for " + c);
					}
					currentMap = (Map<Character, Object>) currentMap.get(c);
					continue;
				} else if (currentMap != rootMap) {
					Object value = currentMap.get(Symbols.SEPARATOR);
					if (value != null) {
						if (debug(REPLACE)) {
							System.out.println("found and appended value " + (String) value);
						}
						builder.append((String) value);
						currentMap = rootMap;
						i--; // repeat current symbol with rootMap
						continue;
					}
				} else if (Symbols.isNumberChar(c)) {
					if (debug(REPLACE)) {
						System.out.println("append " + c);
					}
					builder.append(c);
					continue;
				}
				throw new PCAnDieWand(ErrorType.INPUT_ERROR, "Illegal character: " + rawEquation.substring(0, i) + " »" + c + "« " + rawEquation.substring(i + 1));
			}
			rawEquation = builder.toString();
		}
		if (debug(REPLACE)) {
			System.out.println("Replacing done: " + rawEquation);
		}
		return invokeNext(rawEquation);
	}
}
