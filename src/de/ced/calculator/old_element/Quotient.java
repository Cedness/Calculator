package de.ced.calculator.old_element;

import de.ced.calculator.PCAnDieWand;

@Deprecated
public class Quotient extends ElementList {

	private final Element dividend;
	private final Element divisor;

	public Quotient(Element dividend, Element divisor, Element... elements) {
		super(elements);
		this.dividend = dividend;
		this.divisor = divisor;
		// System.out.println(hashCode() + " de.ced.calculator.elements.Quotient
		// createt! " + dividend + " " + divisor + " " + Arrays.toString(elements));
	}

	@Override
	public double calculate() throws PCAnDieWand {
		double result = dividend.calculate() / divisor.calculate();
		for (Element element : elements) {
			result /= element.calculate();
		}
		return result;
	}

	@Override
	public String symbol() {
		return null;
	}

	@Override
	public String info() {
		return null;
	}
}
