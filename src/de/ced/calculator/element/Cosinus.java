package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Cosinus extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.cos(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "cos";
	}

	@Override
	public String info() {
		return "Calculates the cosinus of a number.";
	}
}
