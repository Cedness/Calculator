package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Arctangent extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.atan(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "atan";
	}

	@Override
	public String info() {
		return "Calculates the arctangent of a number.";
	}
}
