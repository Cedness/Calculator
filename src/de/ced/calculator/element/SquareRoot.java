package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class SquareRoot extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.sqrt(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "sqrt";
	}

	@Override
	public String info() {
		return "Calculates the square root of a number. This this the number, which result into this number when multiplied to itself.";
	}
}
