package de.ced.calculator.element;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import de.ced.calculator.PCAnDieWand;

public class Ceil extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return v.round(new MathContext(0, RoundingMode.UP));
	}

	@Override
	public String symbol() {
		return "ceil";
	}

	@Override
	public String info() {
		return "Rounds the number up to the next int-value.";
	}
}
