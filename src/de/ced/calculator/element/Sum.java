package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class Sum extends Operator {

	@Override
	protected BigDecimal operate(BigDecimal a, BigDecimal b) throws PCAnDieWand {
		return a.add(b);
	}

	@Override
	public String symbol() {
		return "+";
	}

	@Override
	public String info() {
		return "Calculates the sum of 2 operands.";
	}
}
