package de.ced.calculator.element;

import java.math.BigDecimal;

import de.ced.calculator.PCAnDieWand;

public class CubeRoot extends OneparamFunction {

	@Override
	protected BigDecimal apply(BigDecimal v) throws PCAnDieWand {
		return new BigDecimal(String.valueOf(Math.cbrt(v.doubleValue())));
	}

	@Override
	public String symbol() {
		return "cbrt";
	}

	@Override
	public String info() {
		return "Calculates the cube root of a number. This is the number, which results into the former number when multiplied two times to itself.";
	}
}
